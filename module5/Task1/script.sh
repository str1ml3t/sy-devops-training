#!/bin/bash

echo "#######################################################
creating list of binaries
#######################################################";
if [ -d /bin ]
    then 
        ls -l /bin/ >> bin.txt
    else
        echo "Binaries hadnt been found"
fi

echo "git@bitbucket.org:str1ml3t/vagrant.git" > url
git clone $(cat url);

echo "export ISSOFT_VAR=https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.5.0-amd64-netinst.iso" >> /etc/environment
source /etc/environment
wget --tries=10 -c $ISSOFT_VAR