#!/bin/bash

set -e # Exit immediately if a simple command exits with a non-zero status
set -x # Activate debugging from here
echo "### ---=== Script started ===--- ###"

### --- === functions === --- ###

function process_name {
  echo "Input process name:"
  read typed
}

function empty_input {
if [ -z "$typed" ]
  then
    echo "No argument supplied"
    exit 1
fi
}

function process_doesnt_find {
  echo "Process doesn't find"
}

function process_finding {
  echo "Process $typed was found"
}

function process_tree {
  echo "Process tree is:"
  ps -f --forest -C $typed 
}

function pid_list {
  echo "Process pids list contains:"
  ps -f -C $typed | grep $typed | awk '{print $2}' | tac | tee pids.txt | xargs -n4
}

function kill_process {
  file=pids.txt
  for pid in $(cat $file) 
  do
    kill -15 $pid 
    echo "Process $pid terminated"
  done
}

### --- === main script === --- ###

process_name
empty_input
if id $typed > /dev/null 2>&1
then
  process_finding
  process_tree
  pid_list
  kill_process
else
  process_doesnt_find
fi
set +x # Stop debugging from here
echo "### ---=== Script comleted ===--- ###"