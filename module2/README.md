#######################################################
Task 1
#######################################################
1. We need some plugins for this task:
vagrant plugin install vagrant-disksize;
vagrant plugin install vagrant-hostmanager";
2. Run command into new terminal window to add vagrant ssh vnc options: "vagrant ssh ubuntu -- "-L 5900:127.0.0.1:5900"". It needs for remote connection.
3. Install ubuntu manually 
4. Run sudo echo "10.10.10.102 ubuntu" >> /etc/hosts && sudo echo "10.10.10.101 centos" >> /etc/hosts

#######################################################
Task2
#######################################################
1. vagrant up
2. ???
3. PROFIT

#######################################################
Task3
#######################################################
0. Select quantity of VMs in variable "VM_COUNT"
1. Vagrant up
2. We may check created VMs by the following command:
vboxmanage list runningvms

"task3_Ubuntu1_1600085175994_71475" {d9897562-d2f7-4da0-ae3e-2fb30dc22de9}
"task3_Ubuntu2_1600086972020_86591" {185e3ed1-de47-4142-9f55-39c54f0ae3ef}
"task3_Ubuntu3_1600089617512_72747" {19033550-9300-41a6-888d-cf3b7e38d18a}
"module3_Ubuntu_1600180233958_28138" {b6112dc4-185f-4955-a1aa-a22f23eb0ccb}
"lesson3_ubuntu1_1600355017627_56135" {7f4a09cb-cc64-49d8-8f0e-3864f786e2cf}
"lesson3_ubuntu2_1600355108217_22473" {4cca8fac-e0e7-44e4-ad17-2f5239016aa3}

3. The best way to create your personal box is select current vm and run the following command:
vagrant suspend ubuntu1 ;vagrant package --base "lesson3_ubuntu1_1600355017627_56135" --output ubuntu1804.box

==> ubuntu1: Saving VM state and suspending execution...
==> lesson3_ubuntu1_1600355017627_56135: Discarding saved state of VM...
==> lesson3_ubuntu1_1600355017627_56135: Clearing any previously set forwarded ports...
==> lesson3_ubuntu1_1600355017627_56135: Exporting VM...
==> lesson3_ubuntu1_1600355017627_56135: Compressing package to: /home/stanislavyakubovsky/Projects/DevOpsTraining/module2/lesson3/ubuntu1804.box

4. And then add created box to our storage:
vagrant box add ubuntu1804.box --name "ubuntu18"

==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'ubuntu18' (v0) for provider: 
    box: Unpacking necessary files from: file:///home/stanislavyakubovsky/Projects/DevOpsTraining/module2/lesson3/ubuntu1804.box
==> box: Successfully added box 'ubuntu18' (v0) for 'virtualbox'!

5.  Lets checkout our new box. Destroy current VMs:
vagrant destroy -f

6. Replacing box images on freshly created
7. Changing variable "VM_COUNT"
8. vagrant up
9. ???
10.
```
 Bringing machine 'ubuntu1' up with 'virtualbox' provider...
==> ubuntu1: Importing base box 'ubuntu 18.04'...
```

#######################################################
Task4
#######################################################
We may create stack of instances with 'vagrant up'
after initialising, testing web-server with 'curl http://localhost:8080/testapp'