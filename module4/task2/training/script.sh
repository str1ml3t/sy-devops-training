#!/bin/bash

git clone https://github.com/vitamin-b12/training;
cd training/;
unzip task.zip;
mkdir -p sorted/{video,audio,books,undefined};
mkdir sorted/video/{80x,200x,latest};
for i {mp3, flac, alac}; do mv *.$i sorted/audio/ ; done
mv *.mp3 sorted/audio/;
mv *.mp4 sorted/video/;
mv *.pdf sorted/books/;
mv *.md  sorted/undefined/;
mv *.chm sorted/undefined/;
cd sorted/video/;
find . -name "*200*" -type f -print0 | xargs -0 mv -t 200x/
find . -name "*198*" -type f -print0 | xargs -0 mv -t 80x/
find . -name "*201*" -type f -print0 | xargs -0 mv -t latest/
cd -;
tar czf ask.completed.tar.gz sorted;
